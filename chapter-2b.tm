<TeXmacs|2.1>

<style|generic>

<\body>
  <doc-data|<doc-title|Lattice of sub-wavelength resonators>>

  We move away from tight-binding models to explore something a bit more
  intricate: a model involving point resonators coupled by electromagnetic
  interactions. The goal of this chapter is to explain the Hamiltonian used
  in the subsequent chapter. For a more complete derivation of this
  Hamiltonian, see the thesis of A. Goetschy [].

  <section|One atom in the vacuum>

  In this section, I consider the case of an atom in vacuum, modeled as a
  two-level system with ground state and excited state energies denoted as
  <math|E<rsub|g>> and <math|E<rsub|e>>, respectively. The wave functions
  associated with these states are denoted as
  <math|<around*|\||g|\<rangle\>>> and <math|<around*|\||e|\<rangle\>>>. The
  resonance frequency of this atom is <math|\<omega\><rsub|0>>, such that we
  have:

  <\equation>
    E<rsub|e>-E<rsub|g>=\<hbar\>\<omega\><rsub|0>
  </equation>

  <\big-figure|<image|figs/2level_int.pdf|0.99par|||>>
    <label|2level_bath>On the left, we depict a two-level atom with a
    three-fold degenerate excited state, represented by the Hamiltonian
    <math|H<rsub|atom>> for this atom. On the right, we have illustrated the
    photon bath in which this atom is immersed; the Hamiltonian represents
    this transverse electric field: <math|H<rsub|trans>>. Finally, the
    interaction between the two is handled by our Hamiltonian
    <math|H<rsub|int>>. In this part we show how the <math|H<rsub|int>>
    finally translates itself into a decay time of
    <math|1/\<Gamma\><rsub|0>>.
  </big-figure>

  In the following, we consider the atoms to be sufficiently small compared
  to the wavelength of the light that might later propagate among them.
  Typically frequencies around <math|\<omega\><rsub|0>> while the spacing
  between the atoms will be at least an order of magnitude bigger than the
  Bohr radii. Because of that, we can treat them as point scatterers. More
  precisely, we can do the so-called electric dipole approximation in which
  we ignore spatial variation of the fields over the size of the atom. the
  electron and the nucleus form a point electric dipole. With this
  assumption, we can write the Hamiltonian in the
  Power-Zienau-Wooley<\footnote>
    The PZW gauge is obtained from the Coulomb gauge by doing the unitary
    transformation <math|U=\<mathe\><rsup|i*q*<math-bf|d>\<cdot\><math-bf|A>/\<hbar\>c>>
  </footnote> gauge for this atom located at <math|<math-bf|r=0>>,
  <reference|2level_bath> abstractly illustrates the different terms of our
  Hamiltonian, showing the two-level atom, the photon bath, and the
  interaction between them:

  <\eqnarray*>
    <tformat|<table|<row|<cell|H>|<cell|=>|<cell|\<hbar\>\<omega\><rsub|0><big|sum><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|g\<alpha\>>+\<hbar\><big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>>\<omega\><rsub|<math-bf|k>>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>a<rsub|<math-bf|k>,\<epsilon\>>-<big|sum><rsub|\<alpha\>><math-bf|D><rsub|\<alpha\>>\<cdummy\><math-bf|E><around*|(|<math-bf|0>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|H<rsub|atom>+H<rsub|trans>+H<rsub|int><eq-number><label|ham-raw>>>>>
  </eqnarray*>

  The sum over <math|\<alpha\>> runs over all excited states. Here because
  our atom is considered as a two-level atom of spin 1, the excited level is
  three times degenerate. This three states can be associated to the three
  directions of space: <math|x>,<math|y> and <math|z>.
  <math|\<sigma\><rsub|\<alpha\>g>> and <math|\<sigma\><rsub|g\<alpha\>>> are
  the spin-operators of the atom defined by\ 

  <\equation>
    \<sigma\><rsub|\<alpha\>g>=<around*|\||\<alpha\>|\<rangle\>><around*|\<langle\>|g|\|><infix-and>\<sigma\><rsub|g\<alpha\>>=<around*|\||g|\<rangle\>><around*|\<langle\>|\<alpha\>|\|>
  </equation>

  These operators are usually called raising and lowering operators. Note
  that <math|\<sigma\><rsub|\<alpha\>g><around*|\||\<alpha\>|\<rangle\>>=0>
  and <math|\<sigma\><rsub|g\<alpha\>><around*|\||g|\<rangle\>>=0> while
  <math|\<sigma\><rsub|\<alpha\>g><around*|\||g|\<rangle\>>=<around*|\||\<alpha\>|\<rangle\>>>
  and <math|\<sigma\><rsub|g\<alpha\>><around*|\||\<alpha\>|\<rangle\>>=<around*|\||g|\<rangle\>>>.

  The second term in <reference|ham-raw> is the Hamiltonian for the
  transverse electromagnetic field, <math|a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>>
  is the creation operator and <math|a<rsub|<math-bf|k>,\<epsilon\>>> is the
  annihilation operator. They both act in the Fock space. We also have
  introduced here <math|\<omega\><rsub|<math-bf|k>>=k*c>. Finally, the last
  term describes the interaction of the atom with the electric field. In the
  dipole approximation, <math|<math-bf|D><rsub|\<alpha\>>> is the dipole
  operator given by:

  <\equation>
    <math-bf|D><rsub|\<alpha\>>=\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>>+<math-bf|d><rsup|\<ast\>><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>|)>=<math-bf|D><rsub|\<alpha\>><rsup|+>+<math-bf|D><rsub|\<alpha\>><rsup|->
  </equation>

  With <math|\<mu\><rsub|0>> the dipolar moment of the atom and the direction
  of the dipole is given by:

  <\equation>
    <math-bf|d><rsub|\<alpha\>>=<frac|<around*|\<langle\>|g|\|><math-bf|l><around*|\||\<alpha\>|\<rangle\>>|<around*|\<\|\|\>|<around*|\<langle\>|g|\|><math-bf|l><around*|\||\<alpha\>|\<rangle\>>|\<\|\|\>>>
  </equation>

  where <math|<math-bf|l>> is internal distance between the electron and the
  nucleus. The electric field can be written by summing over the reciprocal
  space vectors and polarizations:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><around*|(|<math-bf|r>|)>>|<cell|=>|<cell|i<big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>>\<epsilon\><sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>><around*|(|a<rsub|<math-bf|k>,\<epsilon\>>\<mathe\><rsup|i<math-bf|k>\<cdummy\><math-bf|r>>-\<epsilon\>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>\<mathe\><rsup|-i<math-bf|k>\<cdummy\><math-bf|r>>|)>=<math-bf|E><rsub|+><around*|(|<math-bf|r>|)>+<math-bf|E><rsub|-><around*|(|<math-bf|r>|)><eq-number>>>|<row|<cell|>|<cell|<below|=|<around*|(|<math-bf|r>=<math-bf|0>|)>>>|<cell|i<big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>>\<epsilon\><sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>><around*|(|a<rsub|<math-bf|k>,\<epsilon\>>-\<epsilon\>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>|)>=<math-bf|E><rsub|+><around*|(|<math-bf|0>|)>+<math-bf|E><rsub|-><around*|(|<math-bf|0>|)>>>>>
  </eqnarray*>

  Where we have separated the electric field into two independent
  contributions, this to facilitate the calculations performed latern and we
  have <math|<math-bf|E><rsub|+><around*|(|<math-bf|r>|)><rsup|\<dagger\>>=<math-bf|E><rsub|-><around*|(|<math-bf|r>|)>>
  as expected because the electric field must be real. Note that this field
  does not represent the total electric field but only the transverse field
  due to the PZW unitary transform, this is a common mistake encountered
  [Katarina]. A more explicit notation could be
  <math|<math-bf|E><rsub|\<perp\>>>, but to avoid further complicating
  already complex notations, we will stick with <math|<math-bf|E>>
  throughout. Our goal will now be to integrate out the degree of freedom of
  the vacuum to focus on the dynamic of the atom, to do so, we start by
  solving the equation of motions for our operator
  <math|a<rsub|<math-bf|k>,\<epsilon\>>>. The Heisenberg equation is given
  by:

  <\equation>
    <wide|a|\<dot\>><rsub|<math-bf|k>,\<epsilon\>>=<frac|i|\<hbar\>><around*|[|H,a<rsub|<math-bf|k>,\<epsilon\>>|]>=-i\<omega\><rsub|<math-bf|k>>a<rsub|<math-bf|k>,\<epsilon\>>+<frac|1|\<hbar\>><big|sum><rsub|\<alpha\>><sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>>\<epsilon\>\<cdummy\><math-bf|D><rsub|\<alpha\>><around*|(|t|)>
  </equation>

  The solution of this differential equation is given by:

  <\equation>
    <label|a-sol>a<rsub|<math-bf|k>,\<epsilon\>><around*|(|t|)>=a<rsub|<math-bf|k>,\<epsilon\>><around*|(|0|)>\<mathe\><rsup|-i\<omega\><rsub|<math-bf|k>>t>+<big|int><rsub|0><rsup|t><big|sum><rsub|\<alpha\>><sqrt|<frac|\<omega\><rsub|<math-bf|k>>|2\<hbar\>\<varepsilon\><rsub|0>V>>\<epsilon\>\<cdummy\><math-bf|D><rsub|\<alpha\>><around*|(|t<rprime|'>|)>\<mathe\><rsup|i\<omega\><rsub|<math-bf|k>><around*|(|t<rprime|'>-t|)>
    >\<mathd\>t<rprime|'>
  </equation>

  \;

  Where the first term is the solution of the homogeneous differential
  equation, representing the case without a source, whereas the second term
  represents the solution in the presence of a source. In our present
  situation the source is the two-level atom. The integral starts at
  <math|t<rprime|'>=0> because we assume the interaction to be tuned on at
  <math|t<rprime|'>=0>. It stops at <math|t<rprime|'>=t> because we only keep
  the retarded solution. We will now apply another approximation, the
  Markovian approximation. For that we assume the atom-field coupling is weak
  enough so that we can consider the evolution of the ordering operators to
  be of the form:

  <\equation>
    \<sigma\><rsub|\<alpha\>g><around*|(|t<rprime|'>|)>=\<sigma\><rsub|\<alpha\>g><around*|(|t|)>\<mathe\><rsup|-i\<omega\><rsub|0><around*|(|t<rprime|'>-t|)>>
  </equation>

  <\equation>
    \<sigma\><rsub|g\<alpha\>><around*|(|t<rprime|'>|)>=\<sigma\><rsub|g\<alpha\>><around*|(|t|)>\<mathe\><rsup|i\<omega\><rsub|0><around*|(|t<rprime|'>-t|)>>
  </equation>

  The intuition behind this approximation is that the field operator has no
  \Pmemory\Q of the operator at a previous time. We can now use this ansatz
  in the second term of the solution <reference|a-sol>:

  <\equation>
    a<rsub|<math-bf|k>,\<epsilon\>><around*|(|t|)>=\<epsilon\>*<sqrt|<frac|\<omega\><rsub|<math-bf|k>>|2\<hbar\>\<varepsilon\><rsub|0>V>><big|sum><rsub|\<alpha\>>\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><big|int><rsub|0><rsup|t>\<mathe\><rsup|i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)><around*|(|t<rprime|'>-t|)>
    >\<mathd\>t<rprime|'>+<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><big|int><rsub|0><rsup|t>\<mathe\><rsup|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)><around*|(|t<rprime|'>-t|)>
    >\<mathd\>t<rprime|'>|)>
  </equation>

  Note that I did not write the homogeneous solution
  <math|a<rsub|<math-bf|k>,\<epsilon\>><around*|(|0|)>\<mathe\><rsup|-i\<omega\><rsub|<math-bf|k>>t>>,
  indeed we will later average on the vacuum and this will make disappear
  this term as we have <math|a<rsub|<math-bf|k>,\<epsilon\>><around*|(|0|)><around*|\||vac|\<rangle\>>=0>.
  I stop writing it now to make things more clear. We will now integrate over
  <math|t<rprime|'>>, keeping in mind that cases
  <math|\<omega\><rsub|<math-bf|k>>=\<pm\>\<omega\><rsub|0>> will have to be
  treaded lightly later on when integrating over
  <math|\<omega\><rsub|<math-bf|k>>>.

  <\equation>
    a<rsub|<math-bf|k>,\<epsilon\>><around*|(|t|)>=\<epsilon\>*<sqrt|<frac|\<omega\><rsub|<math-bf|k>>|2\<hbar\>\<varepsilon\><rsub|0>V>><big|sum><rsub|\<alpha\>>\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)>>+<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>>|)>
  </equation>

  We can now subtitue in the transverse electric field:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><rsub|+><around*|(|<math-bf|r>=<math-bf|0>,t|)>>|<cell|=>|<cell|i<big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>>\<mathe\><rsup|i<math-bf|k>\<cdummy\><math-bf|r>><sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>>\<epsilon\><around*|(|\<epsilon\>*<sqrt|<frac|\<omega\><rsub|<math-bf|k>>|2\<hbar\>\<varepsilon\><rsub|0>V>><big|sum><rsub|\<alpha\>>\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)>>+|\<nobracket\>>|\<nobracket\>>>>|<row|<cell|>|<cell|+>|<cell|<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>><around*|\<nobracket\>|<around*|\<nobracket\>||)>|)><eq-number>>>>>
  </eqnarray*>

  We can now compute the sum on the polarization using
  <math|<big|sum>\<epsilon\>\<otimes\>\<epsilon\>=<with|font|Bbb|1>-<math-bf|k>\<otimes\><math-bf|k>/k<rsup|2>>,
  we end up with:

  <\equation>
    <math-bf|E><rsub|+><around*|(|<math-bf|0>,t|)>=\<mu\><rsub|d><big|sum><rsub|\<alpha\>><big|sum><rsub|<math-bf|k>><frac|\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V><around*|(|<with|font|Bbb|1>-<frac|<math-bf|k>\<otimes\><math-bf|k>|k<rsup|2>>|)><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)>>+<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>>|)>
  </equation>

  Before going any further we will go in the mode continuum limit, doing the
  transformation:

  <\equation>
    <big|sum><rsub|<math-bf|k>>\<rightarrow\><frac|V|<around*|(|2\<pi\>|)><rsup|3>><big|int>\<mathd\><rsup|3><math-bf|k>
  </equation>

  We get:

  <\equation>
    <math-bf|E><rsub|+><around*|(|<math-bf|0>,t|)>=<frac|\<mu\><rsub|d>|<around*|(|2\<pi\>|)><rsup|3>><big|int><big|sum><rsub|\<alpha\>><frac|\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>><around*|(|<with|font|Bbb|1>-<frac|<math-bf|k>\<otimes\><math-bf|k>|k<rsup|2>>|)><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)>>+<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>>|)>\<mathd\><rsup|3><math-bf|k>
  </equation>

  Then we use spherical coordinate to rewrite our integral:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><rsub|+><around*|(|<math-bf|><math-bf|0>,t|)>>|<cell|=>|<cell|<frac|\<mu\><rsub|d>|<around*|(|2\<pi\>|)><rsup|3>><big|int><rsub|0><rsup|\<infty\>><big|int><big|sum><rsub|\<alpha\>><frac|\<omega\><rsub|>|2\<varepsilon\><rsub|0>><around*|(|<with|font|Bbb|1>-<frac|<math-bf|k>\<otimes\><math-bf|k>|k<rsup|2>>|)><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><frac|1-\<mathe\><rsup|i<around*|(|\<omega\><rsub|>+\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|>+\<omega\><rsub|0>|)>>|\<nobracket\>><eq-number>>>|<row|<cell|>|<cell|+>|<cell|<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><frac|1-\<mathe\><rsup|i<around*|(|\<omega\><rsub|>-\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|>-\<omega\><rsub|0>|)>><around*|\<nobracket\>||)>\<mathd\>\<Omega\>k<rsup|2>\<mathd\>k>>>>
  </eqnarray*>

  The integral on <math|\<mathd\>\<Omega\>> gives a factor
  <math|4\<pi\>\<times\>2/3> and we subtitute in the integral using the
  relation <math|k=\<omega\>/c> :

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><rsub|+><around*|(|<math-bf|r>=<math-bf|0>,t|)>>|<cell|=>|<cell|<frac|\<mu\><rsub|d>|<around*|(|2\<pi\>|)><rsup|3>><frac|2\<times\>4\<pi\>|3><big|int><rsub|0><rsup|\<infty\>><big|sum><rsub|\<alpha\>><frac|\<omega\>|2\<varepsilon\><rsub|0>><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|>+\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\>+\<omega\><rsub|0>|)>>+<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\>-\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|>-\<omega\><rsub|0>|)>>|)>k<rsup|2>\<mathd\>k>>|<row|<cell|>|<cell|=>|<cell|<frac|\<mu\><rsub|d>|\<pi\><rsup|2>><frac|1|3><big|int><rsub|0><rsup|\<infty\>><big|sum><rsub|\<alpha\>><frac|\<omega\>c|2\<varepsilon\><rsub|0>><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|>+\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\>+\<omega\><rsub|0>|)>>+<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\>-\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|>-\<omega\><rsub|0>|)>>|)><frac|\<omega\><rsup|2>\<mathd\>\<omega\>|c<rsup|3>><eq-number>>>>>
  </eqnarray*>

  We are now ready to compute the integral over <math|\<omega\>>, the
  attentive reader can now notice that the integrand is divergent for
  <math|\<omega\>=\<omega\><rsub|0>>. I will focus for a few lines only on
  the integral to explain how I treat this divergence. First we should notice
  the following if we try to compute the integral over <math|t<rprime|'>>:

  <\equation>
    <frac|<around*|(|1-\<mathe\><rsup|-i<around*|(|\<omega\>-\<omega\><rsub|0>|)>t>|)>|\<omega\>-\<omega\><rsub|0>>=<frac|<around*|(|1-cos<around*|(||\<nobracket\>><around*|(|\<omega\>-\<omega\><rsub|0>|)>t|)><around*|\<nobracket\>||)>|\<omega\>-\<omega\><rsub|0>>+i<frac|sin<around*|(|<around*|(|\<omega\>-\<omega\><rsub|0>|)>t|)>|\<omega\>-\<omega\><rsub|0>>
  </equation>

  The first term behaves as <math|<around*|(|\<omega\>-\<omega\><rsub|0>|)><rsup|-1>>
  because <math|<around*|(|1-cos<around*|(||\<nobracket\>><around*|(|\<omega\>-\<omega\><rsub|0>|)>t|)>>
  is bounded and is zero when <math|\<omega\>=\<omega\><rsub|0>>. The second
  term is zero except when <math|\<omega\>=\<omega\><rsub|0>>. The treatment
  of this integral is a standard result in QED, see [reference], where we
  introduce <math|\<cal-P\>> as the Cauchy principal part and the delta
  function <math|\<delta\>>:

  <\equation>
    <big|int><rsub|0><rsup|\<infty\>>\<omega\><rsup|3><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|>-\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\>-\<omega\><rsub|0>|)>>\<mathd\>\<omega\>\<rightarrow\>\<cal-P\><around*|(|<frac|\<omega\><rsup|3>|\<omega\>-\<omega\><rsub|0>>|)>+i\<pi\>\<omega\><rsub|0><rsup|3>
  </equation>

  and similarly:

  <\equation>
    <big|int><rsub|0><rsup|\<infty\>>\<omega\><rsup|3><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|>-\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\>-\<omega\><rsub|0>|)>>\<mathd\>\<omega\>\<rightarrow\>\<cal-P\><around*|(|<frac|\<omega\><rsup|3>|\<omega\>+\<omega\><rsub|0>>|)>
  </equation>

  \;

  The Cauchy principal part is the divergent term corresponding to the
  lambshift. It is considered in the following to be part of
  <math|\<omega\><rsub|0>>, the frequency resonance of our atom. It is
  finally possible to finish this cumbersome computation:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><rsub|+><around*|(|<math-bf|r>=<math-bf|0>,t|)>>|<cell|=>|<cell|<frac|\<mu\><rsub|d>|\<pi\><rsup|2>><frac|1|3><frac|1|c<rsup|3>2\<varepsilon\><rsub|0>><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)>\<cal-P\><around*|(|<frac|\<omega\><rsup|3>|\<omega\>+\<omega\><rsub|0>>|)>+<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><around*|(|\<cal-P\><around*|(|<frac|\<omega\><rsup|3>|\<omega\>-\<omega\><rsub|0>>|)>+i\<pi\>\<omega\><rsub|0><rsup|3>|)><eq-number>>>>>
  </eqnarray*>

  The two principal parts of Cauchy can now be reintegrated into our
  resonance frequency <math|\<omega\><rsub|0>>. Indeed, this divergence stems
  from the interaction of the atom with the vacuum, but experimentally, it is
  known that this divergence is actually finite. Therefore, we integrate it
  directly into <math|\<omega\><rsub|0>>, this is known as the Lamb shift.

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><rsub|+><around*|(|<math-bf|r>=<math-bf|0>,t|)>>|<cell|=>|<cell|<frac|i\<mu\><rsub|d>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><eq-number>>>>>
  </eqnarray*>

  We will now reintroduce this expression of <math|<math-bf|E><rsub|>> into
  the interaction Hamiltonian. It is at this point that we can average over
  the vacuum, which removes the term involving
  <math|a<rsub|<math-bf|k>,\<epsilon\>><around*|(|0|)>> as I mentioned
  earlier in the text. This average also makes the term <math|H<rsub|trans>>
  (see <reference|ham-raw>) because averaging over the vacuum removes terms
  linear in <math|a<rsub|<math-bf|k>,\<epsilon\>>> on
  <math|a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>>. The use of the
  normal order allows us to retrieve the more easily the semi-classical limit
  of the Heisenberg equations. \ We will now apply a last, but not least,
  approximation to finish the computation, the rotating wave approximation
  (RWA). Because we have assumed <math|\<sigma\><rsub|\<alpha\>g><around*|(|t<rprime|'>|)>=\<sigma\><rsub|\<alpha\>g><around*|(|t|)>\<mathe\><rsup|-i\<omega\><rsub|0><around*|(|t<rprime|'>-t|)>>>,
  in the beginning of this text. If we do the product given in
  <reference|before-rwa>, then the terms <math|\<sigma\><rsub|g\<alpha\>>\<sigma\><rsub|g\<beta\>>>
  and <math|\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|\<beta\>g>> will
  oscillate at frequency of <math|2\<omega\><rsub|0>> and
  <math|-2\<omega\><rsub|0>>, consequently they will oscillate too fast
  compared to non oscillating term and can be ignored. That's the RWA:

  <\eqnarray*>
    <tformat|<table|<row|<cell|H<rsub|int>>|<cell|=>|<cell|<big|sum><rsub|\<alpha\>><math-bf|D><rsub|\<alpha\>><rsup|+>\<cdummy\><math-bf|E><rsup|+><around*|(|<math-bf|0>|)>+<math-bf|D><rsub|\<alpha\>><rsup|->\<cdummy\><math-bf|E><rsup|-><around*|(|<math-bf|0>|)>=>>|<row|<cell|>|<cell|=>|<cell|<frac|i\<mu\><rsub|d>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><big|sum><rsub|\<alpha\>,\<beta\>><math-bf|D><rsub|\<alpha\>><rsup|+>\<cdot\><math-bf|D><rsub|\<beta\>><rsup|+>+<math-bf|D><rsup|-><rsub|\<alpha\>>\<cdot\><math-bf|D><rsub|\<beta\>><rsup|->=<frac|i\<mu\><rsub|d><rsup|2>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><big|sum><rsub|\<alpha\>,\<beta\>><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><math-bf|d><rsub|\<beta\>><rsup|\<ast\>>\<sigma\><rsub|\<beta\>g>+<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><math-bf|d><rsub|\<beta\>><rsup|>\<sigma\><rsub|g\<beta\>><eq-number>>>>>
  </eqnarray*>

  \;

  And now I consider the dipolar moment to be purely real to simplify, a more
  precise treatment could be given for others problems:

  <\eqnarray*>
    <tformat|<table|<row|<cell|H<rsub|int>>|<cell|=>|<cell|<frac|i\<mu\><rsub|d><rsup|2>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><big|sum><rsub|\<alpha\>,\<beta\>><math-bf|d><rsub|\<alpha\>>\<cdummy\><math-bf|d><rsub|\<beta\>><around*|(|\<sigma\><rsub|g\<alpha\>>\<sigma\><rsub|\<beta\>g>+\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|g\<beta\>>|)>\<delta\><rsub|\<alpha\>\<beta\>>>>|<row|<cell|>|<cell|=>|<cell|<frac|i\<mu\><rsub|d><rsup|2>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>>\<cdummy\><math-bf|d><rsub|\<alpha\>><around*|\<nobracket\>|<around*|(|\<sigma\><rsub|g\<alpha\>>\<sigma\><rsub|\<alpha\>g>+\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|g\<alpha\>>|\<nobracket\>>|)><eq-number>>>>>
  </eqnarray*>

  and since the linewidth is defined by:

  <\equation>
    \<Gamma\><rsub|0>=<frac|\<mu\><rsub|d><rsup|2>k<rsub|0><rsup|3>|3\<pi\>\<hbar\>\<varepsilon\><rsub|0>>=<frac|\<mu\><rsub|d><rsup|2>|3\<pi\>\<hbar\>\<varepsilon\><rsub|0>><around*|(|<frac|\<omega\><rsub|0>|c>|)><rsup|3>
  </equation>

  we get for the Hamiltonian of interaction:

  <\eqnarray*>
    <tformat|<table|<row|<cell|H<rsub|int>>|<cell|=>|<cell|i<frac|\<Gamma\><rsub|0>|2>\<hbar\><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>>\<cdummy\><math-bf|d><rsub|\<alpha\>><around*|\<nobracket\>|<around*|(|\<sigma\><rsub|g\<alpha\>>\<sigma\><rsub|\<alpha\>g>+\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|g\<alpha\>>|\<nobracket\>>|)>>>|<row|<cell|>|<cell|=>|<cell|i<frac|\<Gamma\><rsub|0>|2>\<hbar\><with|font|Bbb|1><rsub|3><eq-number>>>>>
  </eqnarray*>

  Our treatment finally gives the following effective Hamiltonian for one
  atom in the vacuum, of frequency resonance <math|\<omega\><rsub|0>> and
  decay time <math|1/\<Gamma\><rsub|0>>:\ 

  <\equation>
    H<rsub|eff>=\<hbar\><matrix|<tformat|<table|<row|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>>|<cell|>|<cell|>>|<row|<cell|>|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>>|<cell|>>|<row|<cell|>|<cell|>|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>>>>>>
  </equation>

  One last thing to consider is to add a magnetic field along the <math|z>
  direction, this will lift the degenracy of the excited state by shifting
  the energy of <math|\<mu\><rsub|B>B> where <math|\<mu\><rsub|B>> is the
  Bohr magneton and <math|B> the intensity of the magnetic field:

  <\equation>
    H<rsub|eff>=\<hbar\><matrix|<tformat|<table|<row|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>-\<mu\><rsub|B>B>|<cell|>|<cell|>>|<row|<cell|>|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>+\<mu\><rsub|B>B>|<cell|>>|<row|<cell|>|<cell|>|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>>>>>>
  </equation>

  \;

  <section|<math|N> atoms in the vacuum>

  To treat a case with more than one atom, more precisely a case of <math|N>
  identical atoms with frequencies <math|\<omega\><rsub|0>> and linewidth
  <math|\<Gamma\><rsub|0>> situated in a lattice, immobile, meaning trapped
  in potential wells of infinite amplitude. We first need to establish how
  the atoms interact with each other via the electromagnetic field. For this
  purpose, we will write down the Maxwell's equations and derive the
  propagator.

  <subsection|Maxwell equations>

  The equations describing the evolution of an electromagnetic field in time
  and space are derived from the works of James C. Maxwell [reference]. These
  equations establish a relationship between the electric field
  <math|<math-bf|E><around*|(|<math-bf|r>,t|)>> [V/m] and the magnetic field
  <math|<math-bf|B><around*|(|<math-bf|r>,t|)>> [W/m<math|<rsup|2>>]. They
  are formulated in a modern form using the divergence and curl operators:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<nabla\>\<cdummy\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|<frac|\<rho\><around*|(|<math-bf|r>,t|)>|\<varepsilon\><rsub|0>><eq-number><label|max-gauss>>>|<row|<cell|\<nabla\>\<cdummy\><math-bf|B><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|0<eq-number><label|max-thomson>>>|<row|<cell|\<nabla\>\<times\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-<frac|\<partial\><math-bf|B><around*|(|<math-bf|r>,t|)>|\<partial\>t><eq-number><label|max-faraday>>>|<row|<cell|\<nabla\>\<times\><math-bf|B><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|\<varepsilon\><rsub|0>\<mu\><rsub|0><frac|\<partial\><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t>+\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)><eq-number><label|max-ampere>>>>>
  </eqnarray*>

  Where <math|\<rho\><around*|(|<math-bf|r>,t|)>> [C/m<math|<rsup|3>>] is the
  total charge density and <math|<math-bf|J><around*|(|<math-bf|r>,t|)>> is
  the current density. These last two are linked by the equation:

  <\equation>
    <frac|\<partial\>\<rho\><around*|(|<math-bf|r>,t|)>|\<partial\>t>+\<nabla\>\<cdummy\><math-bf|J><around*|(|<math-bf|r>,t|)>=0<label|continuite>
  </equation>

  and <math|\<varepsilon\><rsub|0>=8.854\<times\>10<rsup|-12>> [F/m] is the
  free-space electric permittivity while <math|\<mu\><rsub|0>=4\<pi\>\<times\>10<rsup|-7>>
  is the free-space magnetic permeability. These two constants are related by
  the equation:

  <\equation>
    \<varepsilon\><rsub|0>\<mu\><rsub|0>=<frac|1|c<rsup|2>>
  </equation>

  where <math|c=3\<times\>10<rsup|-9>> [m/s] is the speed of light in vacuum.\ 

  <subsection|Wave equation in Fourier space>

  If we take the curl of <reference|max-faraday> we get:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<nabla\>\<times\>\<nabla\>\<times\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-<frac|\<partial\>\<nabla\>\<times\><math-bf|B><around*|(|<math-bf|r>,t|)>|\<partial\>t><eq-number>>>|<row|<cell|<below|\<Rightarrow\>|using
    <reference|max-ampere>>\<nabla\><around*|(|\<nabla\>\<cdummy\><math-bf|E><around*|(|<math-bf|r>,t|)>|)>-\<Delta\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-<frac|\<partial\>|\<partial\>t><around*|(|\<varepsilon\><rsub|0>\<mu\><rsub|0><frac|\<partial\><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t>+\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)>|)><eq-number>>>|<row|<cell|<below|\<Rightarrow\>|using
    <reference|max-gauss>>\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|\<rho\><around*|(|<math-bf|r>,t|)>|\<varepsilon\><rsub|0>>|)>-\<Delta\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-\<varepsilon\><rsub|0>\<mu\><rsub|0><frac|\<partial\><rsup|2><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t<rsup|2>>-\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)><eq-number>>>|<row|<cell|\<Rightarrow\>\<Delta\><math-bf|E><around*|(|<math-bf|r>,t|)>-<frac|1|c<rsup|2>><frac|\<partial\><rsup|2><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t<rsup|2>>>|<cell|=>|<cell|\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)>+\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|\<rho\><around*|(|<math-bf|r>,t|)>|\<varepsilon\><rsub|0>>|)><eq-number>>>>>
  </eqnarray*>

  We now introduce the Fourier transform of the involved fields:

  <\equation>
    <math-bf|E><around*|(|<math-bf|r>,t|)>=<frac|1|2\<pi\>><big|int><rsub|-\<infty\>><rsup|+\<infty\>><math-bf|E><around*|(|<math-bf|r>,\<omega\>|)>\<mathe\><rsup|-i\<omega\>t>\<mathd\>\<omega\>
  </equation>

  <\equation>
    <math-bf|B><around*|(|<math-bf|r>,t|)>=<frac|1|2\<pi\>><big|int><rsub|-\<infty\>><rsup|+\<infty\>><math-bf|B><around*|(|<math-bf|r>,\<omega\>|)>\<mathe\><rsup|-i\<omega\>t>\<mathd\>\<omega\>
  </equation>

  We obtain:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<below|\<Rightarrow\>|using
    <reference|continuite>><around*|(|\<Delta\>+k<rsub|0><rsup|2>|)><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)>-\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|<math-bf|J><around*|(|<math-bf|r>,t|)>|i\<omega\>\<varepsilon\><rsub|0>>|)><eq-number>>>|<row|<cell|\<Rightarrow\><around*|(|\<Delta\>+k<rsub|0><rsup|2>|)><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|\<mu\><rsub|0>i\<omega\><around*|(|<math-bf|J><around*|(|<math-bf|r>,t|)>+\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|<math-bf|J><around*|(|<math-bf|r>,t|)>|*k<rsub|0><rsup|2>>|)>|)><eq-number>>>>>
  </eqnarray*>

  Where we introduced <math|k<rsub|0>=\<omega\>/c>.

  <subsection|Green propagator>

  The solution of\ 

  <\equation>
    <around*|(|\<Delta\>+k<rsub|0><rsup|2>|)><math-bf|G><around*|(|<math-bf|r>,<math-bf|r><rprime|'>|)>=-\<delta\><around*|(|<math-bf|r>-<math-bf|r><rsup|<rprime|'>>|)>
  </equation>

  is given for spherical homogeneous and isotropic medium by\ 

  <\equation>
    G<rsub|0><rsup|\<pm\>><around*|(|r|)>=<frac|\<mathe\><rsup|\<pm\>i*k<rsub|0>r>|4\<pi\>r>
  </equation>

  If we apply on this last equation the operator
  <math|<with|font|Bbb|1>+<frac|\<nabla\>\<nabla\>|k<rsub|0><rsup|2>>> we
  get:

  \;

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|\<nabla\>\<nabla\>|k<rsub|0><rsup|2>>G<rsub|0><rsup|\<pm\>><around*|(|r|)>>|<cell|=>|<cell|<around*|(|-1-<frac|2i|k<rsub|0>r>+<frac|2|k<rsub|0><rsup|2>r<rsup|2>>|)>G<rsub|0><rsup|\<pm\>><around*|(|r|)><eq-number>>>>>
  </eqnarray*>

  After a bit of algebra, we can introduce the functions:

  <\equation>
    P<around*|(|x|)>=1-<frac|1|x>+<frac|1|x<rsup|2>>
    <infix-and>Q<around*|(|x|)>=-1+<frac|3|x>-<frac|3|x<rsup|2>>
  </equation>

  and write the dyadic Green's function describing the coupling of atoms by
  electromagnetic waves as:

  <\equation>
    \<cal-G\><around*|(|<math-bf|r><rsub|1>,<math-bf|r><rsub|2>|)>=G<rsub|0><rsup|><around*|(|r|)><around*|(|P<around*|(|k<rsub|0>r|)><with|font|Bbb|1><rsub|3>+<frac|<math-bf|r>\<otimes\><math-bf|r>|r<rsup|2>>Q<around*|(|k<rsub|0>r|)>|)>
  </equation>

  Where we have set <math|<math-bf|r>=<math-bf|r><rsub|1>-<math-bf|r><rsub|2>>
  and <math|r=<around*|\||<math-bf|r>|\|>>. Note that this expression
  diverges in if <math|r<rsub|1>> becomes close of <math|r<rsub|2>>. To
  circumvent this problem, we introduce a small spherical volume of
  exclusion:

  <\equation>
    \<cal-G\><around*|(|<math-bf|r><rsub|1>,<math-bf|r><rsub|2>|)>=<frac|\<delta\><around*|(|<math-bf|r>|)>|3k<rsub|0><rsup|2>>-G<rsub|0><rsup|><around*|(|r|)><around*|(|P<around*|(|k<rsub|0>r|)><with|font|Bbb|1><rsub|3>+<frac|<math-bf|r>\<otimes\><math-bf|r>|r<rsup|2>>Q<around*|(|k<rsub|0>r|)>|)>
  </equation>

  <subsection|Effective Hamiltonian for <math|N> atoms>

  <\big-figure|<image|figs/2level_int_N.pdf||||>>
    <label|2level_bath_N>The interaction between atoms is governed by the
    real part of <math|\<cal-G\><around*|(|<math-bf|r>|)>>, while it's the
    imaginary part that handles the interaction with the electric field. This
    is where we find the <math|\<Gamma\><rsub|0>> from the previous part.
  </big-figure>

  We will now extend the calculation from the first section to the case of
  <math|N> atoms. We can use the previous section to obtain the interactions
  between the sites. We start by rewriting the Hamiltonian, which is
  identical to <reference|ham-raw>, except for the sum over the sites:

  <\eqnarray*>
    <tformat|<table|<row|<cell|H>|<cell|=>|<cell|\<hbar\>\<omega\><rsub|0><big|sum><rsub|<math-bf|r><rsub|i>><big|sum><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g><rsup|i>\<sigma\><rsup|i><rsub|g\<alpha\>>+\<hbar\><big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>>\<omega\><rsub|<math-bf|k>>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>a<rsub|<math-bf|k>,\<epsilon\>>-<big|sum><rsub|<math-bf|r><rsub|i>><big|sum><rsub|\<alpha\>><math-bf|D><rsub|i\<alpha\>>\<cdummy\><math-bf|E><around*|(|<math-bf|r><rsub|i>|)><eq-number>>>>>
  </eqnarray*>

  Similarly, we also rewrite the electric field, unlike in the first section
  where we considered <math|<math-bf|r>=0>, but now for an arbitrary
  <math|<math-bf|r>>. Again we split this field in two:

  <\equation>
    <tabular|<tformat|<table|<row|<cell|<math-bf|E><around*|(|<math-bf|r><rsub|>|)>>|<cell|=>|<cell|i<big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>><around*|(|<sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>>\<epsilon\>a<rsub|<math-bf|k>,\<epsilon\>>\<mathe\><rsup|i<math-bf|k>\<cdummy\><math-bf|r><rsub|>>-\<epsilon\>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>\<mathe\><rsup|-i<math-bf|k>\<cdummy\><math-bf|r>>|)>=<math-bf|E><rsub|+><around*|(|<math-bf|r>|)>+<math-bf|E><rsub|-><around*|(|<math-bf|r>|)>>>>>>
  </equation>

  The dipolar moment is now defined for every sites, of course the
  spin-operators are introduced for every sites <math|i>:

  <\equation>
    <math-bf|D><rsub|i\<alpha\>>=\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsup|i><rsub|g\<alpha\>>+<math-bf|d><rsup|\<ast\>><rsub|\<alpha\>>\<sigma\><rsup|i><rsub|\<alpha\>g>|)>
  </equation>

  The resolution of the Heisenberg equations is almost unchanged, with the
  addition of a term involving <math|\<mathe\><rsup|-i<math-bf|k>\<cdummy\><math-bf|r><rsub|i>>>:

  <\equation>
    <wide|a|\<dot\>><rsub|<math-bf|k>,\<epsilon\>>=<frac|i|\<hbar\>><around*|[|H,a<rsub|<math-bf|k>,\<epsilon\>>|]>=-i\<omega\><rsub|<math-bf|k>>a<rsub|<math-bf|k>,\<epsilon\>>+<frac|1|\<hbar\>><big|sum><rsub|<math-bf|r><rsub|i>><big|sum><rsub|\<alpha\>><sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>>\<epsilon\>\<cdummy\><math-bf|D><rsub|i\<alpha\>><around*|(|t|)>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>\<mathe\><rsup|-i<math-bf|k>\<cdummy\><math-bf|r><rsub|i>>
  </equation>

  We can then reintroduce this expression for
  <math|a<rsub|<math-bf|k>,\<epsilon\>>> into the electric field, giving:

  <\equation>
    <math-bf|E><rsub|+><around*|(|<math-bf|r>,t|)>=<frac|-\<mu\><rsub|d>|<around*|(|2\<pi\>|)><rsup|3>><big|int><big|sum><rsub|<math-bf|r><rsub|i>,\<alpha\>><frac|\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>><around*|(|<with|font|Bbb|1>-<frac|<math-bf|k>\<otimes\><math-bf|k>|k<rsup|2>>|)><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><big|int><rsub|0><rsup|t>\<mathe\><rsup|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)><around*|(|t<rprime|'>-t|)>
    >\<mathd\>t<rprime|'>\<mathe\><rsup|-i<math-bf|k>\<cdummy\><around*|(|<math-bf|r>-<math-bf|r><rsub|i>|)>>+h.c.|)>\<mathd\><rsup|3><math-bf|k>
  </equation>

  We now recognize the Fourier transform of the Green's function calculated
  in the previous section. By substituting this expression into the field, we
  finish the calculation in a similar manner to the first part. Finally, we
  obtain:

  <\equation>
    <block*|<tformat|<table|<row|<cell|H<rsub|eff>=<big|sum><rsub|<math-bf|r><rsub|i>,\<alpha\>>\<hbar\><around*|(|\<omega\><rsub|0>-i<frac|\<Gamma\><rsub|0>|2>|)>\<sigma\><rsub|\<alpha\>g><rsup|i>\<sigma\><rsup|i><rsub|g\<alpha\>>+<frac|3\<pi\>\<hbar\>c\<Gamma\><rsub|0>|\<omega\><rsub|0>><big|sum><rsub|<math-bf|r><rsub|i>\<neq\><math-bf|r><rsub|j>><big|sum><rsub|<math-bf|>\<alpha\>,\<beta\>><math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<cdummy\><math-bf|\<cal-G\>><around*|(|<math-bf|r>-<math-bf|r><rsub|i>|)>\<cdummy\><math-bf|d><rsub|\<beta\>>\<sigma\><rsub|\<alpha\>g><rsup|i>\<sigma\><rsup|j><rsub|g\<beta\>>>>>>>
  </equation>

  This formula is illustrated by <reference|2level_bath_N> where the distinct
  roles of the real and imaginary parts of
  <math|<math-bf|\<cal-G\>><around*|(|<math-bf|r>|)>> are clearly seen:
  atom-atom interaction for the real part and interaction of the atoms with
  the field for the imaginary part.

  <section|One atom in a Fabry-P�rot Cavity>

  \;

  <section*|Bib>

  Antezza Castin

  Milloni

  Cohen tanoudji

  Katarina\ 

  Goetshy
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|2level_bath|<tuple|1|1>>
    <associate|2level_bath_N|<tuple|2|7>>
    <associate|a-sol|<tuple|9|2>>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-10|<tuple|3|8>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|5>>
    <associate|auto-4|<tuple|2.1|5>>
    <associate|auto-5|<tuple|2.2|6>>
    <associate|auto-6|<tuple|2.3|6>>
    <associate|auto-7|<tuple|2.4|7>>
    <associate|auto-8|<tuple|2|7>>
    <associate|auto-9|<tuple|3|8>>
    <associate|continuite|<tuple|35|6>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|ham-raw|<tuple|3|1>>
    <associate|max-ampere|<tuple|34|5>>
    <associate|max-faraday|<tuple|33|5>>
    <associate|max-gauss|<tuple|31|5>>
    <associate|max-thomson|<tuple|32|5>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        On the left, we depict a two-level atom with a three-fold degenerate
        excited state, represented by the Hamiltonian
        <with|mode|<quote|math>|H<rsub|atom>> for this atom. On the right, we
        have illustrated the photon bath in which this atom is immersed; the
        Hamiltonian represents this transverse electric field:
        <with|mode|<quote|math>|H<rsub|trans>>. Finally, the interaction
        between the two is handled by our Hamiltonian
        <with|mode|<quote|math>|H<rsub|int>>. In this part we show how the
        <with|mode|<quote|math>|H<rsub|int>> finally translates itself into a
        decay time of <with|mode|<quote|math>|1/\<Gamma\><rsub|0>>.
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        The interaction between atoms is governed by the real part of
        <with|mode|<quote|math>|\<cal-G\><around*|(|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|r>>>|)>>,
        while it's the imaginary part that handles the interaction with the
        electric field. This is where we find the
        <with|mode|<quote|math>|\<Gamma\><rsub|0>> from the previous part.
      </surround>|<pageref|auto-8>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>One
      atom in the vacuum> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc><with|mode|<quote|math>|N>
      atoms in the vacuum> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>

      <with|par-left|<quote|1tab>|2.1<space|2spc>Maxwell equations
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1tab>|2.2<space|2spc>Wave equation in Fourier
      space <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|2.3<space|2spc>Green propagator
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|1tab>|2.4<space|2spc>Effective Hamiltonian for
      <with|mode|<quote|math>|N> atoms <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>One
      atom in a Fabry-P�rot Cavity> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Bib>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>