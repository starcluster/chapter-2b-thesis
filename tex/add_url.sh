#!/bin/bash

# Vérifier si le fichier de références LaTeX est fourni en argument
if [ $# -ne 1 ]; then
    echo "Usage: $0 <fichier_tex>"
    exit 1
fi

# Stocker le nom du fichier
fichier_tex="$1"

# Créer un fichier temporaire pour stocker les modifications
fichier_temp=$(mktemp)

# Parcours du fichier de références LaTeX
while IFS= read -r ligne; do
    # Vérifier si la ligne contient "doi = {un_doi}"
    if [[ $ligne == *"doi = {"* ]]; then
        # Modifier la ligne pour ajouter une virgule à la fin
        ligne_modifiee="${ligne%,*},"
        echo "$ligne_modifiee" >> "$fichier_temp"
        # Extraire le DOI
        doi=$(echo "$ligne" | grep -oP '(?<=doi = \{)[^\}]+')
        # Ajouter la ligne note avec le lien DOI
        echo "    note = {\\url{https://doi.org/$doi}}" >> "$fichier_temp"
    else
        # Écrire la ligne inchangée dans le fichier temporaire
        echo "$ligne" >> "$fichier_temp"
    fi
done < "$fichier_tex"

# Remplacer le fichier original par le fichier temporaire avec les modifications
mv "$fichier_temp" "$fichier_tex"

echo "Modifications terminées avec succès dans $fichier_tex."
