<TeXmacs|2.1>

<style|generic>

<\body>
  <\eqnarray*>
    <tformat|<table|<row|<cell|H>|<cell|=>|<cell|\<hbar\><around*|(|\<omega\><rsub|0>+\<delta\>\<omega\><rsub|0><rsup|<around*|(|1|)>>|)><big|sum><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|g\<alpha\>>+\<hbar\><big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>>\<omega\><rsub|<math-bf|k>>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>a<rsub|<math-bf|k>,\<epsilon\>>-<big|sum><rsub|\<alpha\>><math-bf|D><rsub|\<alpha\>>\<cdummy\><math-bf|E><around*|(|<math-bf|0>|)><eq-number>>>>>
  </eqnarray*>

  <\equation>
    \<sigma\><rsub|\<alpha\>g>=<around*|\||\<alpha\>|\<rangle\>><around*|\<langle\>|g|\|><infix-and>\<sigma\><rsub|g\<alpha\>>=<around*|\||g|\<rangle\>><around*|\<langle\>|\<alpha\>|\|>
  </equation>

  <\equation>
    <math-bf|D><rsub|\<alpha\>>=\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>>+<math-bf|d><rsup|\<ast\>><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>|)>=<math-bf|D><rsub|\<alpha\>><rsup|+>+<math-bf|D><rsub|\<alpha\>><rsup|->
  </equation>

  <\equation>
    <math-bf|d><rsub|\<alpha\>>=<frac|<around*|\<langle\>|g|\|><math-bf|l><around*|\||\<alpha\>|\<rangle\>>|<around*|\<\|\|\>|<around*|\<langle\>|g|\|><math-bf|l><around*|\||\<alpha\>|\<rangle\>>|\<\|\|\>>>
  </equation>

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><around*|(|<math-bf|r>|)>>|<cell|=>|<cell|i<big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>>\<epsilon\><sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>><around*|(|a<rsub|<math-bf|k>,\<epsilon\>>\<mathe\><rsup|i<math-bf|k>\<cdummy\><math-bf|r>>-a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>\<mathe\><rsup|-i<math-bf|k>\<cdummy\><math-bf|r>>|)>=<math-bf|E><rsub|+><around*|(|<math-bf|r>|)>+<math-bf|E><rsub|-><around*|(|<math-bf|r>|)><eq-number>>>>>
  </eqnarray*>

  <\equation>
    <wide|a|\<dot\>><rsub|<math-bf|k>,\<epsilon\>>=<frac|i|\<hbar\>><around*|[|H,a<rsub|<math-bf|k>,\<epsilon\>>|]>=-i\<omega\><rsub|<math-bf|k>>a<rsub|<math-bf|k>,\<epsilon\>>+<frac|1|\<hbar\>><big|sum><rsub|\<alpha\>><sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>>\<epsilon\>\<cdummy\><math-bf|D><rsub|\<alpha\>><around*|(|t|)>
  </equation>

  <\equation>
    <label|a-sol>a<rsub|<math-bf|k>,\<epsilon\>><around*|(|t|)>=a<rsub|<math-bf|k>,\<epsilon\>><around*|(|0|)>\<mathe\><rsup|-i\<omega\><rsub|<math-bf|k>>t>+<big|int><rsub|0><rsup|t><big|sum><rsub|\<alpha\>><sqrt|<frac|\<omega\><rsub|<math-bf|k>>|2\<hbar\>\<varepsilon\><rsub|0>V>>\<epsilon\>\<cdummy\><math-bf|D><rsub|\<alpha\>><around*|(|t<rprime|'>|)>\<mathe\><rsup|i\<omega\><rsub|<math-bf|k>><around*|(|t<rprime|'>-t|)>
    >\<mathd\>t<rprime|'>
  </equation>

  <\equation>
    a<rsub|<math-bf|k>,\<epsilon\>><around*|(|t|)>=\<epsilon\>*<sqrt|<frac|\<omega\><rsub|<math-bf|k>>|2\<hbar\>\<varepsilon\><rsub|0>V>><big|sum><rsub|\<alpha\>>\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><big|int><rsub|0><rsup|t>\<mathe\><rsup|i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)><around*|(|t<rprime|'>-t|)>
    >\<mathd\>t<rprime|'>+<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><big|int><rsub|0><rsup|t>\<mathe\><rsup|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)><around*|(|t<rprime|'>-t|)>
    >\<mathd\>t<rprime|'>|)>
  </equation>

  <\equation>
    a<rsub|<math-bf|k>,\<epsilon\>><around*|(|t|)>=\<epsilon\>*<sqrt|<frac|\<omega\><rsub|<math-bf|k>>|2\<hbar\>\<varepsilon\><rsub|0>V>><big|sum><rsub|\<alpha\>>\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)>>+<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><frac|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>>|)>
  </equation>

  \;

  <\equation>
    <wide|\<sigma\><rsub|\<alpha\>g>|\<dot\>>=<frac|i|\<hbar\>><around*|[|H,\<sigma\><rsub|\<alpha\>g>|]>
  </equation>

  <\equation>
    <wide|\<sigma\><rsub|\<alpha\>g>|\<dot\>>=<frac|i|\<hbar\>><around*|[|\<hbar\><around*|(|\<omega\><rsub|0>+\<delta\>\<omega\><rsub|0><rsup|<around*|(|1|)>>|)><big|sum><rsub|\<beta\>>\<sigma\><rsub|\<beta\>g>\<sigma\><rsub|g\<beta\>>+\<hbar\><big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>>\<omega\><rsub|<math-bf|k>>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>a<rsub|<math-bf|k>,\<epsilon\>>-<big|sum><rsub|\<alpha\>><math-bf|D><rsub|\<alpha\>>\<cdummy\><math-bf|E><around*|(|<math-bf|0>|)>,\<sigma\><rsub|\<alpha\>g>|]>
  </equation>

  \;

  <\equation>
    <wide|\<sigma\><rsub|\<alpha\>g>|\<dot\>>=<frac|i|\<hbar\>>\<hbar\><around*|(|\<omega\><rsub|0>+\<delta\>\<omega\><rsub|0><rsup|<around*|(|1|)>>|)><big|sum><rsub|\<beta\>><around*|[|\<sigma\><rsub|\<beta\>g>\<sigma\><rsub|g\<beta\>>,\<sigma\><rsub|\<alpha\>g>|]>-<big|sum><rsub|\<beta\>><math-bf|E><around*|(|<math-bf|0>|)>\<cdummy\><around*|[|<math-bf|D><rsub|\<beta\>>,\<sigma\><rsub|\<alpha\>g>|]>
  </equation>

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><around*|(|<math-bf|0>|)>>|<cell|=>|<cell|i<big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>>\<epsilon\><sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>><around*|(|\<epsilon\>*<sqrt|<frac|\<omega\><rsub|<math-bf|k>>|2\<hbar\>\<varepsilon\><rsub|0>V>><big|sum><rsub|\<alpha\>>\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><big|int><rsub|0><rsup|t>\<mathe\><rsup|i<around*|(|\<omega\><rsub|<math-bf|k>>+\<omega\><rsub|0>|)><around*|(|t<rprime|'>-t|)>
    >\<mathd\>t<rprime|'>+<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><big|int><rsub|0><rsup|t>\<mathe\><rsup|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)><around*|(|t<rprime|'>-t|)>
    >\<mathd\>t<rprime|'>|)>-a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>|)><eq-number>>>>>
  </eqnarray*>

  =<text-dots>

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><rsub|+><around*|(|<math-bf|r>=<math-bf|0>,t|)>>|<cell|=>|<cell|<frac|i\<mu\><rsub|d>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)><eq-number>>>>>
  </eqnarray*>

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><around*|(|<math-bf|0>|)>>|<cell|=>|<cell|<frac|i\<mu\><rsub|d>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)>+<math-bf|d><rsub|\<alpha\>><rsup|>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><eq-number>>>>>
  </eqnarray*>

  <\equation*>
    <wide|\<sigma\><rsub|\<alpha\>g>|\<dot\>>=<frac|i|\<hbar\>>\<hbar\><around*|(|\<omega\><rsub|0>+\<delta\>\<omega\><rsub|0><rsup|<around*|(|1|)>>|)><big|sum><rsub|\<beta\>><around*|[|\<sigma\><rsub|\<beta\>g>\<sigma\><rsub|g\<beta\>>,\<sigma\><rsub|\<alpha\>g>|]>-<big|sum><rsub|\<beta\>><frac|i\<mu\><rsub|d>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><big|sum><rsub|\<gamma\>><around*|(|<math-bf|d><rsub|\<gamma\>><rsup|\<ast\>>\<sigma\><rsub|\<gamma\>g><around*|(|t|)>+<math-bf|d><rsub|\<gamma\>><rsup|>\<sigma\><rsub|g\<gamma\>><around*|(|t|)>|)>\<cdummy\><around*|[|<math-bf|D><rsub|\<beta\>>,\<sigma\><rsub|\<alpha\>g>|]>
  </equation*>

  RWA

  <\equation*>
    <around*|[|<math-bf|D><rsub|\<beta\>>,\<sigma\><rsub|\<alpha\>g>|]>=<around*|[|\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<beta\>>\<sigma\><rsub|g\<beta\>>+<math-bf|d><rsup|\<ast\>><rsub|\<beta\>>\<sigma\><rsub|\<beta\>g>|)>,\<sigma\><rsub|\<alpha\>g>|]>=\<mu\><rsub|d><around*|[|<math-bf|d><rsub|\<beta\>>\<sigma\><rsub|g\<beta\>>,\<sigma\><rsub|\<alpha\>g>|]>=\<mu\><rsub|d><math-bf|d><rsub|\<beta\>><around*|(|\<sigma\><rsub|g*g>-\<sigma\><rsub|\<alpha\>\<alpha\>>|)>
  </equation*>

  \;

  <\equation*>
    <wide|\<sigma\><rsub|\<alpha\>g>|\<dot\>>=<frac|i|\<hbar\>>\<hbar\><around*|(|\<omega\><rsub|0>+\<delta\>\<omega\><rsub|0><rsup|<around*|(|1|)>>|)><big|sum><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>-<big|sum><rsub|\<alpha\>><frac|i\<mu\><rsub|d>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><big|sum><rsub|\<beta\>><around*|(|<math-bf|d><rsub|\<beta\>><rsup|\<ast\>>\<sigma\><rsub|\<beta\>g><around*|(|t|)>+<math-bf|d><rsub|\<beta\>><rsup|>\<sigma\><rsub|g\<beta\>><around*|(|t|)>|)>\<cdummy\>\<mu\><rsub|d><math-bf|d><rsub|\<alpha\>><around*|(|\<sigma\><rsub|g*g>-\<sigma\><rsub|\<alpha\>\<alpha\>>|)>
  </equation*>

  <\equation*>
    <wide|\<sigma\><rsub|\<alpha\>g>|\<dot\>>=<frac|i|\<hbar\>>\<hbar\><around*|(|\<omega\><rsub|0>+\<delta\>\<omega\><rsub|0><rsup|<around*|(|1|)>>|)><big|sum><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>-<big|sum><rsub|\<alpha\>><frac|i\<mu\><rsub|d>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><around*|(|<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)>+<math-bf|d><rsub|\<alpha\>><rsup|>\<sigma\><rsub|g\<alpha\>><around*|(|t|)>|)>\<cdummy\>\<mu\><rsub|d><math-bf|d><rsub|\<alpha\>><around*|(|\<sigma\><rsub|g*g>-\<sigma\><rsub|\<alpha\>\<alpha\>>|)>
  </equation*>

  <\equation*>
    <wide|\<sigma\><rsub|\<alpha\>g>|\<dot\>>=<frac|i|\<hbar\>>\<hbar\><around*|(|\<omega\><rsub|0>+\<delta\>\<omega\><rsub|0><rsup|<around*|(|1|)>>|)>\<sigma\><rsub|\<alpha\>g>-<frac|i\<mu\><rsub|d>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><around*|(|<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)>+<math-bf|d><rsub|\<alpha\>><rsup|>\<sigma\><rsub|g\<alpha\>><around*|(|t|)>|)>\<cdummy\>\<mu\><rsub|d><math-bf|d><rsub|\<alpha\>><around*|(|\<sigma\><rsub|g*g>-\<sigma\><rsub|\<alpha\>\<alpha\>>|)>
  </equation*>

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|g*g>>|<cell|=>|<cell|\<sigma\><rsub|\<alpha\>g>>>|<row|<cell|\<sigma\><rsub|g\<alpha\>>\<sigma\><rsub|g*g>>|<cell|=>|<cell|0>>|<row|<cell|\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|\<alpha\>\<alpha\>>>|<cell|=>|<cell|0>>|<row|<cell|\<sigma\><rsub|g\<alpha\>>\<sigma\><rsub|\<alpha\>\<alpha\>>>|<cell|=>|<cell|\<sigma\><rsub|g\<alpha\>>>>>>
  </eqnarray*>

  <\equation*>
    <wide|\<sigma\><rsub|\<alpha\>g>|\<dot\>>=<frac|i|\<hbar\>>\<hbar\><around*|(|\<omega\><rsub|0>+\<delta\>\<omega\><rsub|0><rsup|<around*|(|1|)>>|)>\<sigma\><rsub|\<alpha\>g>-<frac|i\<mu\><rsub|d><rsup|2>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><around*|(|<math-bf|d><rsub|\<alpha\>><rsup|\<ast\>><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)>-<math-bf|d><rsub|\<alpha\>><rsup|><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)>|)>
  </equation*>

  <\equation*>
    <wide|\<sigma\><rsub|\<alpha\>g>|\<dot\>>=i<around*|(|\<omega\><rsub|0>+\<delta\>\<omega\><rsub|0><rsup|<around*|(|1|)>>|)>\<sigma\><rsub|\<alpha\>g>+<frac|1|\<hbar\>><frac|\<mu\><rsub|d><rsup|2>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><math-bf|d><rsub|\<alpha\>><rsup|\<ast\>><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)>
  </equation*>

  <\equation*>
    <wide|\<sigma\><rsub|\<alpha\>g>|\<dot\>>=i<around*|(|\<omega\><rsub|0>+\<delta\>\<omega\><rsub|0><rsup|<around*|(|1|)>>|)>\<sigma\><rsub|\<alpha\>g>+<frac|1|\<hbar\>><frac|\<mu\><rsub|d><rsup|2>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)>
  </equation*>

  <\equation*>
    <wide|\<sigma\><rsub|\<alpha\>g>|\<dot\>>=<around*|(|i\<omega\><rsub|0>+<frac|1|\<hbar\>><frac|\<mu\><rsub|d><rsup|2>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>>|)>\<sigma\><rsub|\<alpha\>g><around*|(|t|)>
  </equation*>

  <\equation*>
    \<sigma\><rsub|\<alpha\>g><around*|(|t|)>=\<sigma\><rsub|\<alpha\>g><around*|(|0|)>exp<around*|(|<around*|(|i\<omega\><rsub|0>+<frac|1|\<hbar\>><frac|\<mu\><rsub|d><rsup|2>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>>|)>t|)>
  </equation*>

  <\equation*>
    \<sigma\><rsub|\<alpha\>g><around*|(|t|)>=\<sigma\><rsub|\<alpha\>g><around*|(|0|)>exp<around*|(|-<frac|i*t|\<hbar\>>H<rsub|eff>|)>
  </equation*>

  N atoms

  <\eqnarray*>
    <tformat|<table|<row|<cell|H>|<cell|=>|<cell|\<hbar\>\<omega\><rsub|0><big|sum><rsub|<math-bf|r><rsub|i>><big|sum><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g><rsup|i>\<sigma\><rsup|i><rsub|g\<alpha\>>+H<rsub|trans>-<big|sum><rsub|<math-bf|r><rsub|i>><big|sum><rsub|\<alpha\>><math-bf|D><rsub|i\<alpha\>>\<cdummy\><math-bf|E><around*|(|<math-bf|r><rsub|i>|)>+<frac|1|2\<varepsilon\><rsub|0>><big|sum><rsub|i\<neq\>j><math-bf|D><rsub|i>\<cdummy\><math-bf|D><rsub|j>\<delta\><around*|(|<math-bf|r><rsub|i>-<math-bf|r><rsub|j>|)><eq-number>>>>>
  </eqnarray*>

  <\equation*>
    <wide|a|\<dot\>><rsub|<math-bf|k>,\<epsilon\>>=<frac|i|\<hbar\>><around*|[|H,a<rsub|<math-bf|k>,\<epsilon\>>|]>=-i\<omega\><rsub|<math-bf|k>>a<rsub|<math-bf|k>,\<epsilon\>>+<frac|1|\<hbar\>><big|sum><rsub|<math-bf|r><rsub|i>><big|sum><rsub|\<alpha\>><sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>>\<epsilon\>\<cdummy\><math-bf|D><rsub|i\<alpha\>><around*|(|t|)>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>\<mathe\><rsup|-i<math-bf|k>\<cdummy\><math-bf|r><rsub|i>>
  </equation*>

  <\equation*>
    <wide|\<sigma\>|\<dot\>><rsub|\<alpha\>g><rsup|i><rsup|>=<frac|i|\<hbar\>><around*|[|\<hbar\>\<omega\><rsub|0><big|sum><rsub|<math-bf|r><rsub|i>><big|sum><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g><rsup|i>\<sigma\><rsup|i><rsub|g\<alpha\>>-<big|sum><rsub|<math-bf|r><rsub|i>><big|sum><rsub|\<alpha\>><math-bf|D><rsub|i\<alpha\>>\<cdummy\><math-bf|E><around*|(|<math-bf|r><rsub|i>|)>+<frac|1|2\<varepsilon\><rsub|0>><big|sum><rsub|i\<neq\>j><math-bf|D><rsub|i>\<cdummy\><math-bf|D><rsub|j>\<delta\><around*|(|<math-bf|r><rsub|i>-<math-bf|r><rsub|j>|)>,\<sigma\><rsub|\<alpha\>g>|]>
  </equation*>

  <\equation*>
    <wide|\<sigma\>|\<dot\>><rsub|\<alpha\>g><rsup|i><rsup|>=<big|sum><rsub|<math-bf|r><rsub|i>>i\<omega\><rsub|0>\<sigma\><rsub|\<alpha\>g><rsup|i>+<frac|i|\<hbar\>><around*|[|-<big|sum><rsub|<math-bf|r><rsub|i>><big|sum><rsub|\<alpha\>><math-bf|D><rsub|i\<alpha\>>\<cdummy\><math-bf|E><around*|(|<math-bf|r><rsub|i>|)>+<frac|1|2\<varepsilon\><rsub|0>><big|sum><rsub|i\<neq\>j><math-bf|D><rsub|i>\<cdummy\><math-bf|D><rsub|j>\<delta\><around*|(|<math-bf|r><rsub|i>-<math-bf|r><rsub|j>|)>,\<sigma\><rsub|\<alpha\>g><rsup|i>|]>
  </equation*>

  <\equation>
    <math-bf|E><rsub|+><around*|(|<math-bf|r><rsub|i>,t|)>=<frac|-\<mu\><rsub|d>|<around*|(|2\<pi\>|)><rsup|3>><big|int><big|sum><rsub|<math-bf|r><rsub|j>,\<alpha\>><frac|\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>><around*|(|<with|font|Bbb|1>-<frac|<math-bf|k>\<otimes\><math-bf|k>|k<rsup|2>>|)><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><big|int><rsub|0><rsup|t>\<mathe\><rsup|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)><around*|(|t<rprime|'>-t|)>
    >\<mathd\>t<rprime|'>\<mathe\><rsup|-i<math-bf|k>\<cdummy\><around*|(|<math-bf|r><rsub|i>-<math-bf|r><rsub|j>|)>>+\<ldots\>|)>\<mathd\><rsup|3><math-bf|k>
  </equation>

  <\equation*>
    \;
  </equation*>

  <\equation>
    <math-bf|E><rsub|+><around*|(|<math-bf|r><rsub|i>,t|)>=<frac|i\<mu\><rsub|d>\<omega\><rsub|0><rsup|3>|6\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><big|sum><rsub|<math-bf|r><rsub|i>,\<alpha\>><math-bf|d><rsub|\<alpha\>><rsup|\<ast\>>\<sigma\><rsub|\<alpha\>g><around*|(|t|)>\<mathe\><rsup|-i\<omega\><rsub|0>\<cdummy\><around*|(|<math-bf|r><rsub|i>-<math-bf|r><rsub|j>|)>/c>+<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)>\<mathe\><rsup|i\<omega\><rsub|0>\<cdummy\><around*|(|<math-bf|r><rsub|i>-<math-bf|r><rsub|j>|)>/c>
  </equation>

  <\equation*>
    <math-bf|E><rsub|\<perp\>><around*|(|<math-bf|r><rsub|i>,t|)>=
  </equation*>

  <\equation*>
    \;
  </equation*>

  <\equation*>
    <big|sum><rsub|\<beta\>><math-bf|D><rsub|i\<beta\>>\<cdummy\><math-bf|E><around*|(|<math-bf|r><rsub|i>|)>=<big|sum><rsub|\<beta\>><math-bf|D><rsub|i\<beta\>>\<cdummy\><math-bf|E><around*|(|<math-bf|r><rsub|i>|)>
  </equation*>

  <\equation*>
    <big|sum><rsub|i\<neq\>j><around*|[|<math-bf|D><rsub|i>\<cdummy\><math-bf|D><rsub|j>\<delta\><around*|(|<math-bf|r><rsub|i>-<math-bf|r><rsub|j>|)>,\<sigma\><rsub|\<alpha\>g><rsup|k>|]>=<big|sum><rsub|i\<neq\>j>\<delta\><around*|(|<math-bf|r><rsub|i>-<math-bf|r><rsub|j>|)><around*|(|<math-bf|D><rsub|i>\<cdummy\><around*|[|<math-bf|D><rsub|j>,\<sigma\><rsub|\<alpha\>g><rsup|k>|]>+<around*|[|<math-bf|D><rsub|i>,\<sigma\><rsub|\<alpha\>g><rsup|k>|]><math-bf|D><rsub|j>|)>
  </equation*>

  <\equation*>
    <math-bf|D><rsub|i>\<cdummy\><around*|[|<math-bf|D><rsub|j>,\<sigma\><rsub|\<alpha\>g><rsup|k>|]>=<around*|(|\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<beta\>><rsup|i>\<sigma\><rsub|g\<beta\>><rsup|i>+<math-bf|d><rsup|i\<ast\>><rsub|\<beta\>>\<sigma\><rsub|\<beta\>g><rsup|i>|)>|)>\<cdummy\><around*|[|\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<gamma\>><rsup|j>\<sigma\><rsub|g\<gamma\>><rsup|j>+<math-bf|d><rsup|j\<ast\>><rsub|\<gamma\>>\<sigma\><rsub|\<gamma\>g><rsup|j>|)>,\<sigma\><rsub|\<alpha\>g><rsup|k>|]>
  </equation*>

  <\equation*>
    =\<mu\><rsub|d><rsup|2><around*|(|<around*|(|<math-bf|d><rsub|\<beta\>><rsup|i>\<sigma\><rsub|g\<beta\>><rsup|i>+<math-bf|d><rsup|i\<ast\>><rsub|\<beta\>>\<sigma\><rsub|\<beta\>g><rsup|i>|\<nobracket\>>|)>\<cdummy\><around*|[|<math-bf|d><rsub|\<gamma\>><rsup|j>\<sigma\><rsub|g\<gamma\>><rsup|j>\<sigma\><rsub|\<alpha\>g><rsup|k>+<math-bf|d><rsup|j\<ast\>><rsub|\<gamma\>>\<sigma\><rsub|\<gamma\>g><rsup|j>\<sigma\><rsub|\<alpha\>g><rsup|k>+\<sigma\><rsub|\<alpha\>g><rsup|k><math-bf|d><rsub|\<gamma\>><rsup|j>\<sigma\><rsub|g\<gamma\>><rsup|j>+\<sigma\><rsub|\<alpha\>g><rsup|k><math-bf|d><rsup|j\<ast\>><rsub|\<gamma\>>\<sigma\><rsub|\<gamma\>g><rsup|j>|]>
  </equation*>

  <section|Put in the thesis>

  We will now write a first-order differential equation over
  <math|\<sigma\><rsub|\<alpha\>g>> in order to identify the effective
  Hamiltonian which obeys the following:

  <\equation>
    \<sigma\><rsub|\<alpha\>g><around*|(|t|)>=\<sigma\><rsub|\<alpha\>g><around*|(|0|)>exp<around*|(|-<frac|i*t|\<hbar\>>H<rsub|eff>|)>
  </equation>

  We start from the Heisenberg equation on <math|\<sigma\><rsub|\<alpha\>g>>:

  <\equation>
    <wide|\<sigma\>|\<dot\>><rsub|\<alpha\>g>=<frac|i|\<hbar\>><around*|[|<wide|H|^>,\<sigma\><rsub|\<alpha\>g>|]>=i\<omega\><rsub|0><big|sum><rsub|\<beta\>><around*|[|\<sigma\><rsub|\<beta\>g>\<sigma\><rsub|g\<beta\>>,\<sigma\><rsub|\<alpha\>g>|]>-<frac|i|\<hbar\>><big|sum><rsub|\<beta\>><math-bf|E><around*|(|<math-bf|0>|)>\<cdummy\><around*|[|<math-bf|D><rsub|\<beta\>>,\<sigma\><rsub|\<alpha\>g>|]>
  </equation>

  And then we use the rotating wave approximation (RWA) to remove
  non-resonant terms:

  <\equation>
    <around*|[|<math-bf|D><rsub|\<beta\>>,\<sigma\><rsub|\<alpha\>g>|]>=<around*|[|\<mu\><rsub|d><around*|(|<math-bf|d><rsub|\<beta\>>\<sigma\><rsub|g\<beta\>>+<math-bf|d><rsup|\<ast\>><rsub|\<beta\>>\<sigma\><rsub|\<beta\>g>|)>,\<sigma\><rsub|\<alpha\>g>|]>=\<mu\><rsub|d><around*|[|<math-bf|d><rsub|\<beta\>>\<sigma\><rsub|g\<beta\>>,\<sigma\><rsub|\<alpha\>g>|]>=\<mu\><rsub|d><math-bf|d><rsub|\<alpha\>><around*|(|\<sigma\><rsub|g*g>-\<sigma\><rsub|\<alpha\>\<alpha\>>|)>
  </equation>

  And applying the projectors also simplifies the other sum over
  <math|\<beta\>>:

  <\equation>
    <wide|\<sigma\>|\<dot\>><rsub|\<alpha\>g>=i\<omega\><rsub|0>\<sigma\><rsub|\<alpha\>g>-<frac|i\<mu\><rsub|d>|\<hbar\>><math-bf|E><around*|(|<math-bf|0>|)>\<cdummy\><math-bf|d><rsub|\<alpha\>><around*|(|\<sigma\><rsub|g*g>-\<sigma\><rsub|\<alpha\>\<alpha\>>|)>
  </equation>

  We will now reinject the expression for the electric field derivated
  earlier:

  <\equation>
    <wide|\<sigma\>|\<dot\>><rsub|\<alpha\>g>=i\<omega\><rsub|0>\<sigma\><rsub|\<alpha\>g>+<frac|\<mu\><rsub|d><rsup|2>\<omega\><rsub|0><rsup|3>|6\<hbar\>\<pi\>c<rsup|3>\<varepsilon\><rsub|0>><big|sum><rsub|\<gamma\>><around*|(|<math-bf|d><rsup|\<ast\>><rsub|\<gamma\>>\<sigma\><rsub|\<gamma\>g>+<math-bf|d><rsub|\<gamma\>><rsup|>\<sigma\><rsub|g\<gamma\>>|)>\<cdummy\><math-bf|d><rsub|\<alpha\>><around*|(|\<sigma\><rsub|g*g>-\<sigma\><rsub|\<alpha\>\<alpha\>>|)>
  </equation>

  and we apply again the RWA so that we finally obtain:

  <\equation>
    <wide|\<sigma\>|\<dot\>><rsub|\<alpha\>g>=i\<omega\><rsub|0>\<sigma\><rsub|\<alpha\>g>+<frac|\<mu\><rsub|d><rsup|2>\<omega\><rsub|0><rsup|3>|6\<hbar\>\<pi\>c<rsup|3>\<varepsilon\><rsub|0>>\<sigma\><rsub|\<alpha\>g>
  </equation>

  Which is the desired equation. We can now identify <math|H<rsub|eff>> and
  write:
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|2level_bath|<tuple|1|1|chapter-2b.tm>>
    <associate|2level_bath_N|<tuple|1|7|chapter-2b.tm>>
    <associate|a-sol|<tuple|7|2|chapter-2b.tm>>
    <associate|auto-1|<tuple|1|1|chapter-2b.tm>>
    <associate|auto-10|<tuple|3|8|chapter-2b.tm>>
    <associate|auto-2|<tuple|1.1|1|chapter-2b.tm>>
    <associate|auto-3|<tuple|1.2|5|chapter-2b.tm>>
    <associate|auto-4|<tuple|1.3|5|chapter-2b.tm>>
    <associate|auto-5|<tuple|1.4|6|chapter-2b.tm>>
    <associate|auto-6|<tuple|1|6|chapter-2b.tm>>
    <associate|auto-7|<tuple|2|7|chapter-2b.tm>>
    <associate|auto-8|<tuple|2|7|chapter-2b.tm>>
    <associate|auto-9|<tuple|2|8|chapter-2b.tm>>
    <associate|continuite|<tuple|30|6|chapter-2b.tm>>
    <associate|footnote-1|<tuple|1|1|chapter-2b.tm>>
    <associate|footnr-1|<tuple|1|1|chapter-2b.tm>>
    <associate|ham-raw|<tuple|2|1|chapter-2b.tm>>
    <associate|max-ampere|<tuple|29|5|chapter-2b.tm>>
    <associate|max-faraday|<tuple|28|5|chapter-2b.tm>>
    <associate|max-gauss|<tuple|26|5|chapter-2b.tm>>
    <associate|max-thomson|<tuple|27|5|chapter-2b.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        On the left, we depict a two-level atom with a three-fold degenerate
        excited state, represented by the Hamiltonian
        <with|mode|<quote|math>|H<rsub|atom>> for this atom. On the right, we
        have illustrated the photon bath in which this atom is immersed; the
        Hamiltonian represents this transverse electric field:
        <with|mode|<quote|math>|H<rsub|trans>>. Finally, the interaction
        between the two is handled by our Hamiltonian
        <with|mode|<quote|math>|H<rsub|int>>. In this part we show how the
        <with|mode|<quote|math>|H<rsub|int>> finally translates itself into a
        decay time of <with|mode|<quote|math>|1/\<Gamma\><rsub|0>>.
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        The interaction between atoms is governed by the real part of
        <with|mode|<quote|math>|\<cal-G\><around*|(|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|r>>>|)>>,
        while it's the imaginary part that handles the interaction with the
        electric field. This is where we find the
        <with|mode|<quote|math>|\<Gamma\><rsub|0>> from the previous part.
      </surround>|<pageref|auto-8>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>One
      atom in the vacuum> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc><with|mode|<quote|math>|N>
      atoms in the vacuum> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>

      <with|par-left|<quote|1tab>|2.1<space|2spc>Maxwell equations
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1tab>|2.2<space|2spc>Wave equation in Fourier
      space <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|2.3<space|2spc>Green propagator
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|1tab>|2.4<space|2spc>Effective Hamiltonian for
      <with|mode|<quote|math>|N> atoms <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>One
      atom in a Fabry-P�rot Cavity> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Bib>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>